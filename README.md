README
======

This repository started out as a private repo for a little project for my "Creating Online Media" class at Brigham Young University-Idaho. I'm just learning HTML again, and it's basically the first time I've started learning HTML5. This is a simple little project, but I'm rather proud of it, and it contains some useful information, so I'm making the repo public (and it was public anyway at Cloud9 IDE). Here are the places you can find the repository:

* [Bitbucket](https://bitbucket.org/lazyrivr/html-elements "Project on Bitbucket")
* [Cloud9](https://c9.io/lazyrivr/html-elements "Project on Cloud9")

You can also see the webpage live by [viewing it on Cloud9](https://c9.io/lazyrivr/html-elements/workspace/index.html "Project webpage live on Cloud9")

LICENSE
=======

The following license terms apply to you but not me:Hi Shyam

1. The code is open source.
2. Any images are a different story. Look for individual LICENSE.md files in each directory containing images to see the individual terms. Ocassionally, an image will be under full copyright, but placed in the repo for convenience. Most of the time, I'll use or create Creative Commons licensed images.

3. Any third-party code used will remain under its original license. The license will be included.
